CREATE TABLE imagedto (
    id bigint NOT NULL PRIMARY KEY,
    name varchar(255),
    file_type varchar(3),
    pic blob 
);

INSERT INTO Customers (
	id,
	name,
	file_type,
	pic
)
VALUES (
	1,
	'pelda.jpg',
	'jpg',
	'0x69'
);