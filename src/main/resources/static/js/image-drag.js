$(document).ready(function () 
{
	document.getElementById('imageSelectButton').oninput = function(e) 
	{
		var backgroundImageSrc = URL.createObjectURL(this.files[0]);
		initCanvasElements(backgroundImageSrc);
		
	};

	//Color pixels to white, which is outside the forground
	function falseCrop(backgroundImgSrc) 
	{
		  $(this).drawImage({
			  source: backgroundImgSrc,
			  x: 200, y: 200,
			  sWidth: 150,
			  sHeight: 150,
			  cropFromCenter: true
			});
	}

	function initCanvasElements(backgroundImgSrc)
	{
		$('canvas').removeLayers();
		
		$('canvas').addLayer({
			draggable: true,
			layer: true,
			type: 'image',
			name: 'backgroundImage',
			index: 0,
			source: backgroundImgSrc,
			x: 150, y: 150
		});
		
		$('canvas').addLayer({
			layer: true,
			type: 'image',
			name: 'forgroundImage',
			index: 1,
			intangible: true,
			source: '/uploads/test-padlock.png',
			x: 200, y: 200	
		});

		$('canvas').drawLayers();
		
		var backgroundLayer = $('canvas').getLayer('backgroundImage');
		//To see easier the border of the .png I lift up the background image while dragging the image
		$('canvas').mouseup(function() {
			$('canvas').moveLayer('backgroundImage', -1);
			$('canvas').drawLayers();
		}).mousedown(function() {
			$('canvas').moveLayer('backgroundImage', 1);
			$('canvas').drawLayers();
		});

	}
});

