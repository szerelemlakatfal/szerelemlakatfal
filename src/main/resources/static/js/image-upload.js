$(function () {
  var token = $("meta[name='_csrf']").attr("content");
  var header = $("meta[name='_csrf_header']").attr("content");
  $(document).ajaxSend(function(e, xhr, options) {
    xhr.setRequestHeader(header, token);
  });
});

$(document).ready(function () {

    $("#btnImageUploadSubmit").click(function (event) {
    	//check if there is an image selected
    	if( document.getElementById("imageSelectButton").files.length == 0 )
    	{
    			alert("Kérjük válasszon ki egy képet mentés előtt!");
    	}

        //stop submit the form, we will post it manually.
    	
        event.preventDefault();
        fire_ajax_submit();

    });

});

function fire_ajax_submit() {
	
	var canvas = $('canvas');
	var url = canvas.getCanvasImage('png');
	var blobBin = atob(url.split(',')[1]);
	var array = [];
	
	for(var i = 0; i < blobBin.length; i++) {
	    array.push(blobBin.charCodeAt(i));
	}
	
	var file = new Blob([new Uint8Array(array)], {type: 'image/png'});
	
	var formdata = new FormData();
	formdata.append("newImage", file);

	//formdata.append("CustomField", "This is some extra data, testing");

    $("#btnImageUploadSubmit").prop("disabled", true);

    $.ajax({
        type: 'POST',
        enctype: 'multipart/form-data',
        url: "/api/upload-image",
        data: formdata,
        //http://api.jquery.com/jQuery.ajax/
        //https://developer.mozilla.org/en-US/docs/Web/API/FormData/Using_FormData_Objects
        processData: false, //prevent jQuery from automatically transforming the data into a query string
        contentType: false,
        cache: false,
        timeout: 60000,
        success: function (formdata) {

            //$("#result").text(data);
            console.log("SUCCESS : ", formdata);
            $("#btnImageUploadSubmit").prop("disabled", false);
            $('canvas').removeLayers();
            alert("A lakat sikeresen feltöltésre került!");
            

        },
        error: function (e) {

           // $("#result").text(e.responseText);
            console.log("ERROR : ", e.responseText ,"Formdata", formdata);
            $("#btnImageUploadSubmit").prop("disabled", false);

        }
    });

}

