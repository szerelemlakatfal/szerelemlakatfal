package hu.szerelemlakatfal.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import hu.szerelemlakatfal.dto.VotesDTO;

public interface VoteRepository extends CrudRepository<VotesDTO, Long>
{
	void save(List<VotesDTO> votes);
}
