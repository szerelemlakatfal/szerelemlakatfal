package hu.szerelemlakatfal.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import hu.szerelemlakatfal.dto.ImageDTO;

public interface ImageRepository extends CrudRepository<ImageDTO, Long>
{
	void save(List<ImageDTO> imageList);
	
	@Query(value= "SELECT * FROM imagedto WHERE owner_id_user_id = ?1", nativeQuery = true)
	List<ImageDTO> findByAsArray(Long userId);
	
	@Transactional
	@Modifying
	@Query(value= "UPDATE imagedto SET is_primary_image = "
			+ "CASE WHEN(image_id = ?1) THEN (true) ELSE (false) END "
			+ "WHERE owner_id_user_id = ?2", 
			nativeQuery = true)
	void savePrimaryImage(Long imageId, Long userId);
	
	@Transactional
	@Modifying
	@Query(value= "UPDATE imagedto SET number_of_votes = number_of_votes + 1 "
			+ "WHERE image_id = ?1", nativeQuery = true)
	void saveVoteForImage(Long imageId);
	
	@Query(value= "SELECT * FROM imagedto WHERE is_primary_image = true", nativeQuery = true)
	List<ImageDTO> findAllPrimaryImage();
	
	@Query(value= "SELECT * FROM imagedto ORDER BY image_name LIMIT 100", nativeQuery = true)
	List<ImageDTO> findAllPublicImages();
	
	@Query(value= "SELECT * FROM imagedto WHERE sec_owner_id = ?1", nativeQuery = true)
	List<ImageDTO> findAllUserImages(String secOwnerId);
	
	ImageDTO findBySecondaryId(String secondaryId);
}
