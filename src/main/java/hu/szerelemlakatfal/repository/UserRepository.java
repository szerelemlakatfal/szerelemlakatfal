package hu.szerelemlakatfal.repository;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import hu.szerelemlakatfal.dto.UserDTO;

public interface UserRepository extends CrudRepository<UserDTO, Long>
{
	UserDTO findByEmail(String email);
	UserDTO findByPrincipalId(String principalId);
	UserDTO findBySeconaryId(String secondaryId);
	
	@Query(value= "SELECT voted_picture_by_user_id FROM userdto WHERE user_id = ?1", nativeQuery = true)
	Long[] getVotedImageIdFromUser(Long userId);
}
