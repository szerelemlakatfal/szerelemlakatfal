package hu.szerelemlakatfal.service;

import java.util.List;
import java.util.Map;

import hu.szerelemlakatfal.dto.ImageDTO;

public interface ImageVoteSerivce
{
	void setLikeForPhotoAndUser(Long imageId);

	List<Map<String, String>> getAllVotablePrimaryImages();
}
