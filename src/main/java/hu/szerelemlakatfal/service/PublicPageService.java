package hu.szerelemlakatfal.service;

import java.util.List;

import hu.szerelemlakatfal.dto.ImageDTO;
import hu.szerelemlakatfal.dto.UserDTO;

public interface PublicPageService
{
	List<ImageDTO> getTheFirstHundredImage();
	UserDTO getUserPublicDatas(String secondaryId);
	List<ImageDTO> getUserPublicImages(String secondaryId);
	ImageDTO getVotableImage(String secondaryId);
}
