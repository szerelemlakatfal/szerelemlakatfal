package hu.szerelemlakatfal.service;

import java.awt.Image;
import java.util.List;
import java.util.Map;

import org.springframework.web.multipart.MultipartFile;

import hu.szerelemlakatfal.dto.ImageDTO;
import hu.szerelemlakatfal.dto.UserDTO;

public interface UserDashboardService
{
	void store(MultipartFile file);
	List<ImageDTO> getAllImages();
	void setImagePrimary(Long imageId);
	UserDTO getUserPrivateDatas();
}
