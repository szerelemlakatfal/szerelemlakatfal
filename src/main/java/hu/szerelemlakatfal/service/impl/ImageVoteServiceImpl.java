package hu.szerelemlakatfal.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import hu.szerelemlakatfal.dto.ImageDTO;
import hu.szerelemlakatfal.dto.UserAdapter;
import hu.szerelemlakatfal.dto.UserDTO;
import hu.szerelemlakatfal.dto.VotesDTO;
import hu.szerelemlakatfal.repository.ImageRepository;
import hu.szerelemlakatfal.repository.VoteRepository;
import hu.szerelemlakatfal.service.ImageVoteSerivce;

@Service
public class ImageVoteServiceImpl implements ImageVoteSerivce
{
	private ImageRepository imgRepo;
	private VoteRepository voteRepo;
	private UserAdapter userAdapter = null;
	private UserDTO user = null;
	
	@Autowired
	public void setUserRepo(VoteRepository voteRepo)
	{
		this.voteRepo = voteRepo;
	}
	
	@Autowired
	public void setImgRepo(ImageRepository imgRepo)
	{
		this.imgRepo = imgRepo;
	}
	
	@Override
	public void setLikeForPhotoAndUser(Long imageId)
	{
		userAdapter = (UserAdapter) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		user = userAdapter.getUserDTO();
        
        if (imageId != null)
		{
        	VotesDTO vote = new VotesDTO();
        	vote.setUserOfVote(user.getId());
        	vote.setVotedImageId(imageId);
			imgRepo.saveVoteForImage(imageId);
			voteRepo.save(vote);
			
			
		}
		
	}

	@Override
	public List<Map<String, String>> getAllVotablePrimaryImages()
	{
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        user = (UserDTO) authentication.getPrincipal();
        
        List<Map<String,String>> imageObjectArray = new ArrayList<>();
		List<ImageDTO> rawList = imgRepo.findAllPrimaryImage();
		
		for(int i=0; i<imgRepo.findAllPrimaryImage().size();i++)
		{
			Map<String,String> imageObjectMap = new HashMap<>();

            imageObjectMap.put("imageUrl", rawList.get(i).getUrl() +"/"+ rawList.get(i).getName());
            imageObjectMap.put("id", Long.toString(rawList.get(i).getId()));
            imageObjectMap.put("fileType", rawList.get(i).getFileType());
            imageObjectMap.put("imageName", rawList.get(i).getName());
            imageObjectMap.put("numberOfVotes", Long.toString(rawList.get(i).getNumberOfVotes()));
            
            imageObjectArray.add(imageObjectMap);
		}
        
		return imageObjectArray;
	}
	
}
