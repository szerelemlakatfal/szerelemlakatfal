package hu.szerelemlakatfal.service.impl;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import hu.szerelemlakatfal.dto.ImageDTO;
import hu.szerelemlakatfal.dto.UserAdapter;
import hu.szerelemlakatfal.dto.UserDTO;
import hu.szerelemlakatfal.repository.ImageRepository;
import hu.szerelemlakatfal.service.UserDashboardService;

@Service
public class UserDashboardServiceImpl implements UserDashboardService
{
	public static Logger LOGGER = Logger.getLogger(UserDashboardServiceImpl.class.getName());
	
	public ImageRepository imgRepo;
	private UserAdapter userAdapter = null;
	private UserDTO user = null;
	
	@Autowired
	public void setImgRepo(ImageRepository imgRepo)
	{
		this.imgRepo = imgRepo;
	}
	/**
	 * Save files into database
	 * @throws  
	 */
	@Override
	public void store(MultipartFile file)
	{
		userAdapter = (UserAdapter) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		user = userAdapter.getUserDTO();
        
//        try 
//        {
//            if (file.isEmpty()) 
//            {
//                throw new IOException("Failed to store empty file " + file.getOriginalFilename());
//            }
//            if (file.getOriginalFilename().contains("..")) 
//            {
//                // This is a security check
//                throw new IOException(
//                        "Cannot store file with relative path outside current directory "
//                                + file.getOriginalFilename());
//            }
        
        try 
        {
            // Get the filename and build the local file path (be sure that the 
            // application have write permissions on such directory)
        	if (file.isEmpty()) 
            {
                  throw new IOException("Failed to store empty file " + file.getOriginalFilename());
            }
            if (file.getOriginalFilename().contains("..")) 
            {
	              // This is a security check
	              throw new IOException(
	                      "Cannot store file with relative path outside current directory "
	                              + file.getOriginalFilename());
            }
            String filename = user.getSeconaryId() +"_"+ RandomStringUtils.random(12, true, true);
//            File convertedFile = new File(filename);
//            convertedFile.createNewFile();

            BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(user.getUserImageFolder() +"/"+ filename +".png"));
            stream.write(file.getBytes());
            stream.close();
            
//            ImageDTO imageModel = new ImageDTO();
//            imageModel.setName(file.getOriginalFilename());
//            imageModel.setFileType(file.getContentType());
//            imageModel.setPic(file.getBytes());
//            imageModel.setOwnerId(user);
            
            ImageDTO imageModel = new ImageDTO();
            imageModel.setSecondaryId(filename);
            imageModel.setName(filename+".png");
            imageModel.setFileType(file.getContentType());
            imageModel.setDescription("TESZT TESZT");
            imageModel.setUploadDate(new SimpleDateFormat("yyyy.MM.dd").format(new Date()));
            imageModel.setModificationDate(new SimpleDateFormat("yyyy.MM.dd").format(new Date()));
            imageModel.setUrl(user.getUserImageFolder() +"/"+ filename +".png");
            imageModel.setOwnerId(user);
            imageModel.setSecondaryOwnerId(user.getSeconaryId());
            imageModel.setNumberOfVotes(0);
            
            imgRepo.save(imageModel);
        }
        catch (Exception e) 
        {
            System.out.println("Failed to store file " + file.getOriginalFilename() +":"+ e);
        }
	}
	/**
	 * This method create a new List, because image need to be encoded to display on front-end.
	 */
	@Override
	public List<ImageDTO> getAllImages()
	{
		
		userAdapter = (UserAdapter) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		user = userAdapter.getUserDTO();

		return imgRepo.findByAsArray(user.getId());
	}
	
	public void setImagePrimary(Long imageId)
	{
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		userAdapter = (UserAdapter) authentication.getPrincipal();
		user = userAdapter.getUserDTO();
        
		if (imageId != null)
		{
			imgRepo.savePrimaryImage(imageId, user.getId());
		}
	}
	
	public UserDTO getUserPrivateDatas() {
		userAdapter = (UserAdapter) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		return userAdapter.getUserDTO();
	}
		
}
