package hu.szerelemlakatfal.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import hu.szerelemlakatfal.dto.ImageDTO;
import hu.szerelemlakatfal.dto.UserDTO;
import hu.szerelemlakatfal.repository.ImageRepository;
import hu.szerelemlakatfal.repository.UserRepository;
import hu.szerelemlakatfal.service.PublicPageService;

@Service
public class PublicPageServiceImpl implements PublicPageService
{
	
	ImageRepository imageRepo;
	UserRepository userRepo;
	
	@Autowired
	public void setImageRepo(ImageRepository imageRepo)
	{
		this.imageRepo = imageRepo;
	}
	
	@Autowired
	public void setUserRepo(UserRepository userRepo)
	{
		this.userRepo = userRepo;
	}

	@Override
	public List<ImageDTO> getTheFirstHundredImage()
	{
		return imageRepo.findAllPublicImages();
	}

	@Override
	public UserDTO getUserPublicDatas(String secondaryId)
	{
		return userRepo.findBySeconaryId(secondaryId);
	}
	
	@Override
	public List<ImageDTO> getUserPublicImages(String secondaryId)
	{	
		return imageRepo.findAllUserImages(secondaryId);
	}

	@Override
	public ImageDTO getVotableImage(String secondaryId)
	{
		return imageRepo.findBySecondaryId(secondaryId);
	}
	
	
	
	
	
}
