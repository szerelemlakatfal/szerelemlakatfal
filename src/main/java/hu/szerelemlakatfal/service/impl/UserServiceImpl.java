package hu.szerelemlakatfal.service.impl;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

import hu.szerelemlakatfal.dto.UserAdapter;
import hu.szerelemlakatfal.dto.UserDTO;
import hu.szerelemlakatfal.enums.RoleType;
import hu.szerelemlakatfal.repository.UserRepository;

@Service
public class UserServiceImpl implements UserDetailsService
{
	public static Logger LOGGER = Logger.getLogger(UserServiceImpl.class.getName());
	
	private Map<String, Object> principalMap;
	
	UserDTO user = null;
	
	UserRepository userRepository;

	@Autowired
	public void setUserRepository(UserRepository userRepository)
	{
		this.userRepository = userRepository;
	}

	@Override
	public UserAdapter loadUserByUsername(String principalId) 
	{
		
		user = (UserDTO) userRepository.findByPrincipalId(principalId);
		LOGGER.info("PRINCIPALID: "+ principalId);
		
		
		if(user == null)
		{
			//New user
			
			//This the user secondary id, it makes the unique workspace and public profiles for them.
			String secondaryUniqueId = String.valueOf(System.currentTimeMillis());
			
			//Unique workspace for user
			File userFolder = new File("uploads"+ "/" + new SimpleDateFormat("yy/MM/dd/").format(new Date()) + secondaryUniqueId +"/");			
            userFolder.mkdirs();
         
            user = new UserDTO();
			
            user.setPrincipalId((String) getPrincipalMap().get("id"));
            user.setSeconaryId(secondaryUniqueId);
            user.setEmail((String) getPrincipalMap().get("email"));
            user.setName((String) getPrincipalMap().get("name"));
            user.setFirstName((String) getPrincipalMap().get("first_name"));
            user.setProfileImage(getProfilePictureUrl(getPrincipalMap().get("picture")));
            user.setUserImageFolder(userFolder.toString());
            user.setRoles(RoleType.ROLE_USER.name());
            user.setRegistrationDate(new SimpleDateFormat("yyyy.MM.dd").format(new Date()));
            user.setLastLogin(new SimpleDateFormat("yyyy.MM.dd").format(new Date()));
            LOGGER.info(user.getRoles());
		}
		else 
        {
			//User already registered 
        	LOGGER.info("User is already registered, just update the last login and profile picture");
        	user.setLastLogin(new SimpleDateFormat("yyyy.MM.dd").format(new Date()));
        	user.setProfileImage(getProfilePictureUrl(getPrincipalMap().get("picture")));
        }
		
		LOGGER.info("Save user {} "+ (String) getPrincipalMap().get("name") +" "+ (String) getPrincipalMap().get("email") +" Current user email: "+ user.getEmail());

		LOGGER.info(user.getRoles());
		saveUser(user);
		return new UserAdapter(user);
	}
	

	public void saveUser(UserDTO user)
	{
		userRepository.save(user);
	}
	
	public Map<String, Object> getPrincipalMap()
	{
		return this.principalMap;
	}

	public void setPrincipalMap(Map<String, Object> map)
	{
		this.principalMap = map;
		
	}
	
	public UserDTO loadUserByPrincipalId(String principal)
	{
		return userRepository.findByPrincipalId(principal);
	}
	
	//Facebook holds the profile picture in a LinkedHashMap (getPrincipalMap.get("picture") and this function retrieve it.
	public String getProfilePictureUrl( Object profilePictureMap)
	{	
		Map< String, Map<String,String> > tempMap = (LinkedHashMap< String, Map<String,String> >) profilePictureMap;
		String pictureUrl = tempMap.get("data").get("url");
		
		if(profilePictureMap.equals(""))
		{
			return "";
		}
		return pictureUrl;
	}

}
