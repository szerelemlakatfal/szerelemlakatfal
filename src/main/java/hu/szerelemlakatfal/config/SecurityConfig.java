package hu.szerelemlakatfal.config;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.servlet.Filter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.oauth2.resource.AuthoritiesExtractor;
import org.springframework.boot.autoconfigure.security.oauth2.resource.PrincipalExtractor;
import org.springframework.boot.autoconfigure.security.oauth2.resource.UserInfoTokenServices;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.oauth2.client.OAuth2ClientContext;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.filter.OAuth2ClientAuthenticationProcessingFilter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableOAuth2Client;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.CompositeFilter;

import hu.szerelemlakatfal.dto.UserDTO;
import hu.szerelemlakatfal.enums.RoleType;
import hu.szerelemlakatfal.service.impl.UserServiceImpl;
/**
 * 
 * @author Bognar Oliver
 * This class contains the core security properties. 
 * Authentications works with OAuth2, without custom resource server.
 *
 */
@Configuration
@EnableOAuth2Client
public class SecurityConfig extends WebSecurityConfigurerAdapter 
{
	private static final Logger LOGGER = Logger.getLogger(SecurityConfig.class.getName());
	@Autowired
	OAuth2ClientContext oauth2ClientContext;
	
	@Autowired
	PrincipalExtractor principalExtractor; 
	
	@Autowired
	AuthoritiesExtractor authoritiesExtractor;
	
	@Autowired
	UserServiceImpl userService;
	
	/**
	 * We assign our custom UserServiceImpl to the built-in Spring Security UserDetailsService.
	 * In this way our custom service and the data object model what we use
	 * place in the Spring Security context.
	 * @param auth
	 * @throws Exception
	 */
	public void configureAuth(AuthenticationManagerBuilder auth) throws Exception
	{
		auth.userDetailsService(userService);
	}
	
	/**
	 * OAuth2 give us the principal of the user after the authetication.
	 * @return UserAdapter(wrap a custom UserDTO) which is extends from the Spring Security built-in UserDetail.
	 */
	@Bean
    public PrincipalExtractor principalExtractor() 
	{  
        return new PrincipalExtractor()
        {

			@Override
			public Object extractPrincipal(Map<String, Object> map)
			{
				userService.setPrincipalMap(map);
				String principalEmail = (String) map.get("id");
				
				LOGGER.info("Email: "+ principalEmail);
				UserDetails simpleUser = userService.loadUserByUsername(principalEmail);
	            return simpleUser;
			}
			
        };
        
    }
	
	/**
	 * If we want to add some extra user role to our system, when we use OAuth2 for authentication,
	 * we have to override the extractAuthorities method in AuthoritiesExtractor and make a new list of role.
	 * In this case every user has one role, so the method return only 1 length list based on the query which is checks user
	 * role. In default everybody is "ROLE_USER".
	 * @return new AuthorityList
	 */
	@Bean
	public AuthoritiesExtractor authoritiesExtractor() 
	{
		return new AuthoritiesExtractor()
		{

					@Override
					public List<GrantedAuthority> extractAuthorities(Map<String, Object> map)
					{
						UserDTO user = userService.loadUserByPrincipalId((String) map.get("id"));
			    		ArrayList<String> list = new ArrayList<>();
			    		list.add(user.getRoles());
			    		
			    		if (!list.isEmpty()) 
			    		{
			    			return AuthorityUtils.commaSeparatedStringToAuthorityList(StringUtils.collectionToCommaDelimitedString(list));
			    		}
			    		return AuthorityUtils.commaSeparatedStringToAuthorityList(RoleType.ROLE_USER.name());
					}
		};
        
	}
	 
	/**
	 * This method configure the end-points and the corresponding roles.
	 */
	protected void configure(HttpSecurity httpSec) throws Exception
	{
		httpSec
			.authorizeRequests()
				.antMatchers(EndPointsList.permitAll).permitAll()
				.antMatchers(EndPointsList.userAll).hasRole("USER")
				.antMatchers(HttpMethod.POST, EndPointsList.userApiAll).hasRole("USER")
		        .antMatchers(HttpMethod.GET, EndPointsList.userApiAll).hasRole("USER")
				.antMatchers(EndPointsList.adminAll).hasRole("ADMIN")
				.anyRequest().authenticated()
			.and()
				.formLogin()
					.loginPage("/")
					.permitAll()
			.and()
				.logout().logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
					.logoutSuccessUrl("/")
					.permitAll()
			.and().addFilterBefore(ssoFilter(), BasicAuthenticationFilter.class);

	}
	
	/**
	 * 
	 * @param client - Which is the authenticator. (Facebook, Google, Twitter etc.)
	 * @param path - The path of the url on our webpage, where the login go trough.
	 * @return
	 */
	private Filter ssoFilter(ClientResources client, String path)
	{
		  OAuth2ClientAuthenticationProcessingFilter filter = new OAuth2ClientAuthenticationProcessingFilter(path);
		  OAuth2RestTemplate template = new OAuth2RestTemplate(client.getClient(), oauth2ClientContext);
		  filter.setRestTemplate(template);
		  UserInfoTokenServices tokenServices = new UserInfoTokenServices(
		      client.getResource().getUserInfoUri(), client.getClient().getClientId());
		  tokenServices.setRestTemplate(template);
		  tokenServices.setPrincipalExtractor(principalExtractor);
		  tokenServices.setAuthoritiesExtractor(authoritiesExtractor);
		  filter.setTokenServices(tokenServices);
		  return filter;
	}
	
	private Filter ssoFilter() 
	{		  
		  CompositeFilter filter = new CompositeFilter();
		  List<Filter> filters = new ArrayList<>();
		  filters.add(ssoFilter(facebook(), "/login/facebook"));
		  filters.add(ssoFilter(google(), "/login/google"));
		  filter.setFilters(filters);
		  return filter;
	}
	
	
	@Bean
	@ConfigurationProperties("google")
	public ClientResources google() 
	{
	  return new ClientResources();
	}

	@Bean
	@ConfigurationProperties("facebook")
	public ClientResources facebook() 
	{
	  return new ClientResources();
	}
}
