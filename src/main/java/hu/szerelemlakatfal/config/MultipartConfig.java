package hu.szerelemlakatfal.config;

import javax.servlet.MultipartConfigElement;

import org.springframework.boot.web.servlet.MultipartConfigFactory;
import org.springframework.context.annotation.Bean;
/**
 * 
 * @author Bognar Oliver
 * This class override the default temporarily file location on the server.
 *
 */
public class MultipartConfig
{
	@Bean
    MultipartConfigElement multipartConfigElement() {
        MultipartConfigFactory factory = new MultipartConfigFactory();
        factory.setLocation("/data/tmp");
        return factory.createMultipartConfig();
    }
}
