package hu.szerelemlakatfal.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@SuppressWarnings("deprecation")
@Configuration
public class WebConfig extends WebMvcConfigurerAdapter
{

	/**
	 * With this method we can map custom urls to the custom command.
	 */
	@Override
	public void addViewControllers(ViewControllerRegistry registry)
	{
		super.addViewControllers(registry);
		//Auth
		registry.addViewController("/login").setViewName("auth/login");
		registry.setOrder(Ordered.HIGHEST_PRECEDENCE);
	}
	
	/**
	 * With this method we can map custom urls to one of the map which is on the server.
	 * If we does Spring Security can build up the url to reach the content from external folder.
	 */
	@Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
		//Szeged szerver
//        registry.addResourceHandler("/uploads/**")
//                .addResourceLocations("file:///G:/Dokumentumok/Programozas/Programok/SpringBoot/szerelemlakatfal/uploads/");
//        registry.addResourceHandler("user/profil/uploads/**")
//        		.addResourceLocations("file:///G:/Dokumentumok/Programozas/Programok/SpringBoot/szerelemlakatfal/uploads/");
//        registry.addResourceHandler("user/	/vote/uploads/**")
//        		.addResourceLocations("file:///G:/Dokumentumok/Programozas/Programok/SpringBoot/szerelemlakatfal/uploads/");
//        registry.addResourceHandler("/api/upload-image")
//				.addResourceLocations("file:///G:/Dokumentumok/Programozas/Programok/SpringBoot/szerelemlakatfal/uploads/");
//		  registry.addResourceHandler("/public/public_profile/uploads/**")
//				.addResourceLocations("file:///G:/Dokumentumok/Programozas/Programok/SpringBoot/szerelemlakatfal/uploads/");
//		  registry.addResourceHandler("/public/public_picture/uploads/**")
//				.addResourceLocations("file:///G:/Dokumentumok/Programozas/Programok/SpringBoot/szerelemlakatfal/uploads/");
		
		  //Vaskut szerver
		  registry.addResourceHandler("/uploads/**")
	         	.addResourceLocations("file:///E:/Dokumentumok/Programozas//STS Project/szerelemlakatfal/uploads/");
		  registry.addResourceHandler("/user/uploads/**")
			 	.addResourceLocations("file:///E:/Dokumentumok/Programozas/STS Project/szerelemlakatfal/uploads/");
		  registry.addResourceHandler("/user/uploads/**")
			 	.addResourceLocations("file:///E:/Dokumentumok/Programozas/STS Project/szerelemlakatfal/uploads/");
		  registry.addResourceHandler("/api/upload-image")
				.addResourceLocations("file:///E:/Dokumentumok/Programozas/STS Project/szerelemlakatfal/uploads/");
		  registry.addResourceHandler("/public/public_profile/uploads/**")
				.addResourceLocations("file:///E:/Dokumentumok/Programozas/STS Project/szerelemlakatfal/uploads/");
		  registry.addResourceHandler("/public/public_picture/uploads/**")
				.addResourceLocations("file:///E:/Dokumentumok/Programozas/STS Project/szerelemlakatfal/uploads/");
	        
    }
	
}
