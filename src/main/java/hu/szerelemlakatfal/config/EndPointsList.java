package hu.szerelemlakatfal.config;

/**
 * 
 * @author Bognar Oliver
 * This is a class that contains a lot of end-point for Spring Security.
 *
 */
public class EndPointsList
{
	public static final String permitAll[] = {
			"/",
			"/public/index/**", 
			"/public/public_profile/**",
			"/public/public_picture/**",
			"/login/**", 
			"/uploads/**",
			"/webjars/**",
			"/css/**",
			"/img/**",
			"/js/**",
			"/scss/**",
			"/vendor/**",
			"error"};
	
	public static final String userAll[] = {
			"/user/profil/**",
			"/user/vote/**"};
	
	public static final String userApiAll[] = {
			"/api/**"};
	
	public static final String adminAll[] = {
			"/admin/admin/**"};
}
