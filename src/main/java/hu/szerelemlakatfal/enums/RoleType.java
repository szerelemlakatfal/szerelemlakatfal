package hu.szerelemlakatfal.enums;

/**
 * Types of roles
 * @author SAMSUNG-PC
 *
 */
public enum RoleType
{
	ROLE_ADMIN,
	ROLE_USER
}
