package hu.szerelemlakatfal.controller;

import java.net.URI;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import hu.szerelemlakatfal.service.UserDashboardService;

@RestController
@RequestMapping("/api")
public class UploadRestController
{
	private static final Logger LOGGER = Logger.getLogger(UploadRestController.class.getName());
	public UserDashboardService userDashService;
	
	@Autowired
	public void setUserDashService(UserDashboardService userDashService)
	{
		this.userDashService = userDashService;
	}

    /**
     * This method can upload the padlocks with rest.
     * @param uploadfile
     * @return
     */
    @SuppressWarnings("unchecked")
	@PostMapping("/upload-image")
    // If not @RestController, uncomment this
    //@ResponseBody
    public ResponseEntity<?> uploadFile(@RequestParam("newImage") MultipartFile uploadfile) {

    	LOGGER.info("Single file upload!");

        if (uploadfile.isEmpty()) {
            return new ResponseEntity("please select a file!", HttpStatus.OK);
        }

        userDashService.store(uploadfile);
        
        HttpHeaders header = new HttpHeaders();
        header.setLocation(URI.create("/profil"));
        
        return new ResponseEntity("Successfully uploaded - " +
                uploadfile.getOriginalFilename(), header, HttpStatus.OK);

    } 

}
