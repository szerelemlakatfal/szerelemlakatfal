package hu.szerelemlakatfal.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import hu.szerelemlakatfal.service.UserDashboardService;
import hu.szerelemlakatfal.service.impl.PublicPageServiceImpl;

@Controller
@RequestMapping("/public")
public class PublicController
{
	public UserDashboardService userDashService;
	public PublicPageServiceImpl publicPageService;
	
	@Autowired
	public void setUserDashService(UserDashboardService userDashService)
	{
		this.userDashService = userDashService;
	}
	
	@Autowired
	public void setPublicPageService(PublicPageServiceImpl publicPageService)
	{
		this.publicPageService = publicPageService;
	}

	/**
	 * With this method we get url of the public profile. With publicProfileId we can populate our model with the public profile's owner information
	 * and his images.
	 * @param publicProfileId - Which user's pictures was clicked.
	 * @param model - user and his pictures
	 * @return
	 */
	@GetMapping("/public_profile/{publicProfileId}")
	public String publicProfile(@PathVariable String publicProfileId, Model model)
	{
		model.addAttribute("userModel",publicPageService.getUserPublicDatas(publicProfileId));
		model.addAttribute("userImageList",publicPageService.getUserPublicImages(publicProfileId));
		
		return "/public/public_profile";
	}
	
	/**
	 * With this method can populate an image profile with all image data.
	 * For this we only need that image secondary id which was clicked on the public profile page.
	 * @param pictureId
	 * @param model
	 * @return
	 */
	@GetMapping("/public_picture/{pictureId}")
	public String publicImage(@PathVariable String pictureId, Model model)
	{
		model.addAttribute("imageVoteModel",publicPageService.getVotableImage(pictureId));
		
		return "/public/public_picture";
	}
	
}
