package hu.szerelemlakatfal.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import hu.szerelemlakatfal.service.ImageVoteSerivce;
import hu.szerelemlakatfal.service.UserDashboardService;

@Controller
@RequestMapping("/admin")
public class AdminController
{
	public UserDashboardService userDashService;
	public ImageVoteSerivce imageVoteService;
	
	@Autowired
	public void setUserDashService(UserDashboardService userDashService)
	{
		this.userDashService = userDashService;
	}
	
	@Autowired
	public void setImageVoteService(ImageVoteSerivce imageVoteService)
	{
		this.imageVoteService = imageVoteService;
	}

	@GetMapping("/admin")
	public String index(Model model)
	{
		model.addAttribute("inProgressText","In progress...");
		return "admin/admin";
	}
	
}
