package hu.szerelemlakatfal.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import hu.szerelemlakatfal.service.impl.PublicPageServiceImpl;

@Controller
@RequestMapping("/")
public class MainController
{
	PublicPageServiceImpl publicPageServiceImpl;
	
	@Autowired
	public void setPublicPageServiceImpl(PublicPageServiceImpl publicPageServiceImpl)
	{
		this.publicPageServiceImpl = publicPageServiceImpl;
	}
	
	/**
	 * Mapping to index.html
	 * @param model - We put the first 100 image into the model to show on the main page.
	 * @return index.html template
	 */
	@GetMapping("/")
	public String index(Model model)
	{
		model.addAttribute("imageList", publicPageServiceImpl.getTheFirstHundredImage());
		return "/public/index";
	}
	
	
}