package hu.szerelemlakatfal.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import hu.szerelemlakatfal.service.ImageVoteSerivce;
import hu.szerelemlakatfal.service.UserDashboardService;

@Controller
@RequestMapping("/user")
public class UserController
{
	public UserDashboardService userDashService;
	public ImageVoteSerivce imageVoteService;
	
	@Autowired
	public void setUserDashService(UserDashboardService userDashService)
	{
		this.userDashService = userDashService;
	}
	
	@Autowired
	public void setImageVoteService(ImageVoteSerivce imageVoteService)
	{
		this.imageVoteService = imageVoteService;
	}
	
	/**
	 * Mapping to profile.html
	 * @param model - Contain the user private information and his uploaded images.
	 * @return 
	 */
	@GetMapping("/profil")
	public String profil(Model model)
	{
		
		model.addAttribute("loggedUserData", userDashService.getUserPrivateDatas());
		model.addAttribute("allImagesArray", userDashService.getAllImages());
		return "/user/profil";
	}
	
	@GetMapping("/vote")
	public String vote(Model model)
	{
		model.addAttribute("primaryPhotoCollectionToVote", imageVoteService.getAllVotablePrimaryImages());
		return "/user/vote";
	}
	
	/**
	 * IT SHOULD BE MODIFIED WHEN FACEBOOK LIKE AND SHARE WORKS IN PUBLIC MODE.
	 * @param imageId
	 * @return
	 */
	@GetMapping("/vote/likePhoto/{imageId}")
	public String likePhoto(@PathVariable("imageId") Long imageId) 
	{        
	    imageVoteService.setLikeForPhotoAndUser(imageId);

	    return "redirect:/user/vote";
	} 
	
	
}
