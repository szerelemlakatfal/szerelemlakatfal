package hu.szerelemlakatfal.dto;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class UserDTO
{	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long userId;
	private String principalId;
	@Column(name = "sec_id")
	private String seconaryId;
	@Column(name = "email")
	private String email;
	@Column(name = "name")
	private String name;
	@Column(name = "first_name")
	private String firstName;
	@Column(name = "active")
	private int active;
	@Column(name = "user_image_folder")
	private String userImageFolder;
	@OneToMany(mappedBy = "ownerId", fetch = FetchType.LAZY)
	private List<ImageDTO> userImages;
	private String role;
	@Column(name = "registration_date")
	private String registrationDate;
	@Column(name = "last_login")
	private String lastLogin;
	@Column(name = "providerProfileImage")
	private String profileImage;
	
	public UserDTO(){}

	public String getUserImageFolder()
	{
		return userImageFolder;
	}

	public void setUserImageFolder(String userImageFolder)
	{
		this.userImageFolder = userImageFolder;
	}

	public List<ImageDTO> getUserImages()
	{
		return userImages;
	}

	public void setUserImages(List<ImageDTO> userImages)
	{
		this.userImages = userImages;
	}

	public Long getId()
	{
		return userId;
	}

	public void setId(Long id)
	{
		this.userId = id;
	}
	
	public String getSeconaryId()
	{
		return seconaryId;
	}

	public void setSeconaryId(String seconaryId)
	{
		this.seconaryId = seconaryId;
	}

	public String getPrincipalId()
	{
		return principalId;
	}

	public void setPrincipalId(String principalId)
	{
		this.principalId = principalId;
	}

	public String getEmail()
	{
		return email;
	}

	public void setEmail(String email)
	{
		this.email = email;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getFirstName()
	{
		return firstName;
	}

	public void setFirstName(String firstName)
	{
		this.firstName = firstName;
	}

	public int getActive()
	{
		return active;
	}

	public void setActive(int active)
	{
		this.active = active;
	}

	public String getRoles()
	{
		return role;
	}

	public void setRoles(String role)
	{
		this.role = role;
	}

	public String getRegistrationDate()
	{
		return registrationDate;
	}

	public void setRegistrationDate(String registrationDate)
	{
		this.registrationDate = registrationDate;
	}

	public String getLastLogin()
	{
		return lastLogin;
	}

	public void setLastLogin(String lastLogin)
	{
		this.lastLogin = lastLogin;
	}

	public String getProfileImage()
	{
		return profileImage;
	}

	public void setProfileImage(String profileImage)
	{
		this.profileImage = profileImage;
	}
	
	
}
