package hu.szerelemlakatfal.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class VotesDTO
{
	@Id
	@GeneratedValue
	@Column(name = "vote_id")
	private Long voteId;
	@Column(name = "voted_image_id")
	private Long votedImageId;
	@Column(name = "user_of_vote")
	private Long userOfVote;
	
	public VotesDTO() {}

	public Long getVoteId()
	{
		return voteId;
	}

	public void setVoteId(Long voteId)
	{
		this.voteId = voteId;
	}

	public Long getVotedImageId()
	{
		return votedImageId;
	}

	public void setVotedImageId(Long votedImageId)
	{
		this.votedImageId = votedImageId;
	}

	public Long getUserOfVote()
	{
		return userOfVote;
	}

	public void setUserOfVote(Long userOfVote)
	{
		this.userOfVote = userOfVote;
	}
	
	
}
