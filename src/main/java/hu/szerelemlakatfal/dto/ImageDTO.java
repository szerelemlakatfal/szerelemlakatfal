package hu.szerelemlakatfal.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class ImageDTO
{
	
    @GeneratedValue
    @Id
    @Column(name = "image_id")
	private Long id;
    @Column(name = "sec_id")
	private String secondaryId;
    @Column(name = "image_name")
	private String name;
    @Column(name = "image_file_type")
	private String fileType;
    @Column(name = "number_of_votes")
	private long numberOfVotes;
    @Column(name = "image_url")
    private String url;
    @Column(name = "upload_date")
	private String uploadDate;
    @Column(name = "modification_date")
	private String modificationDate;
    @Column(name = "description")
    private String description;
    @Column(name = "sec_owner_id")
	private String secondaryOwnerId;
	@ManyToOne
	private UserDTO ownerId;

	public ImageDTO(){}
    
	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

//	public byte[] getPic()
//	{
//		return pic;
//	}
//
//	public void setPic(byte[] pic)
//	{
//		this.pic = pic;
//	}

	public String getFileType()
	{
		return fileType;
	}

	public void setFileType(String fileType)
	{
		this.fileType = fileType;
	}

	public UserDTO getOwnerId()
	{
		return ownerId;
	}

	public void setOwnerId(UserDTO ownerId)
	{
		this.ownerId = ownerId;
	}

	public long getNumberOfVotes()
	{
		return numberOfVotes;
	}

	public void setNumberOfVotes(long numberOfVotes)
	{
		this.numberOfVotes = numberOfVotes;
	}

	public String getUrl()
	{
		return url;
	}

	public void setUrl(String url)
	{
		this.url = url;
	}

	public String getSecondaryId()
	{
		return secondaryId;
	}

	public void setSecondaryId(String secondaryId)
	{
		this.secondaryId = secondaryId;
	}

	public String getUploadDate()
	{
		return uploadDate;
	}

	public void setUploadDate(String uploadDate)
	{
		this.uploadDate = uploadDate;
	}

	public String getModificationDate()
	{
		return modificationDate;
	}

	public void setModificationDate(String modificationDate)
	{
		this.modificationDate = modificationDate;
	}

	public String getDescription()
	{
		return description;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public String getSecondaryOwnerId()
	{
		return secondaryOwnerId;
	}

	public void setSecondaryOwnerId(String secondaryOwnerId)
	{
		this.secondaryOwnerId = secondaryOwnerId;
	}
	
}
