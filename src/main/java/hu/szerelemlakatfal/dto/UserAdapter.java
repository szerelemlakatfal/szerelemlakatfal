package hu.szerelemlakatfal.dto;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.logging.Logger;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import hu.szerelemlakatfal.enums.RoleType;

public class UserAdapter implements UserDetails
{
	private static final long serialVersionUID = -7997754140090038455L;
	
	private static final Logger LOGGER = Logger.getLogger(UserAdapter.class.getName());
	
	private UserDTO userDTO;
	
	public UserAdapter(){}
	public UserAdapter(UserDTO userDTO)
	{
		this.userDTO = userDTO;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities()
	{
		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
        LOGGER.info("Auth user query role"+ userDTO.getRoles());
        switch(userDTO.getRoles()) 
        {
        	case "USER":  
        	{
        		authorities.add(new SimpleGrantedAuthority(RoleType.ROLE_USER.name()));
        		LOGGER.info("Auth option: "+ RoleType.ROLE_USER.name());
        		break;
        	}
        	
        	case "ADMIN": 
        	{
        		authorities.add(new SimpleGrantedAuthority(RoleType.ROLE_ADMIN.name()));
        		LOGGER.info("Auth option: "+ RoleType.ROLE_ADMIN.name());
        		break;
        	}
        	default: 
        	{
        		authorities.add(new SimpleGrantedAuthority(RoleType.ROLE_USER.name()));
        		break;
        	}
        }
        return authorities;
	}

	@Override
	public String getPassword()
	{
		// TODO Auto-generated method stub
		return RandomStringUtils.random(12, true, true);
	}

	@Override
	public String getUsername()
	{
		// TODO Auto-generated method stub
		return userDTO.getEmail();
	}

	@Override
	public boolean isAccountNonExpired()
	{
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isAccountNonLocked()
	{
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired()
	{
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isEnabled()
	{
		// TODO Auto-generated method stub
		return true;
	}
	public UserDTO getUserDTO()
	{
		return userDTO;
	}
	
	

}
